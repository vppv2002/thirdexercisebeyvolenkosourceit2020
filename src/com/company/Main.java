package com.company;

public class Main {

    public static void main(String[] args) {
        String Str = new String("Дан текст. Нужно подсчитать количество слов и предложений в тексте. Результат вывести в консоль!\n");
        int words = 0;
        int sentence = 0;
        for (String retval : Str.split(" ")) {
            words++;
        }
        for (String retval : Str.split("[\\.\\!\\?] ")) {
            sentence++;
        }
        System.out.println("Количество слов: " + words);
        System.out.println("Количество предложений: " + sentence);
    }
}
